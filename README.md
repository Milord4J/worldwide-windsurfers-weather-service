==============================================================
Worldwide Windsurfer’s Weather Service - 16 Day Weather Forecast API 
==============================================================
This API returns better windsurfing conditions on that day in the 16 forecast day range of one the of following locations:
Jastarnia (Poland)
Bridgetown (Barbados)
Fortaleza (Brazil)
Pissouri (Cyprus)
Le Morne (Mauritius)

All parameters should be supplied to the Weather API as query string parameters.

Base URL
HTTP: 54.174.164.209:8080/WWWAPI/forecast/daily
Supported Methods: GET
Request Parameters
token=[token] (REQUIRED)
token - Your API Key.
==============================================================
API works on AWS Cloud 
Server:Apache Tomcat/8.5.61
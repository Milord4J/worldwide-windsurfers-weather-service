/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wwws.controller;

import com.wwws.service.Services;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.wwws.domain.Forecast;
import org.springframework.web.bind.annotation.RequestMapping;
/**
 *
 * @author USER
 */
@RestController
@RequestMapping("forecast/")
public class ForecastController {
    
    @Autowired
    Services iService;
    
    @GetMapping(value = "daily")
    public Forecast getDailyForecast(@RequestParam String token){
        
        Forecast response = iService.getDailyForecast(token);

        return response;
    }

}

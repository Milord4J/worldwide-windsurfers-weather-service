/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wwws.service;


import com.wwws.domain.Forecast;
import com.wwws.util.OpenFeignClient;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author USER
 */
@Service
public class Services {

//    @Autowired
//    ICurrencyRepository repository;

    @Autowired
    OpenFeignClient feignClient;

    public Forecast getDailyForecast(String token) {
        Forecast forecast = null;
        Double best = 0.0;
        final String[] cities = {"Jastarnia", 
                                 "Bridgetown", 
                                 "Fortaleza", 
                                 "Pissouri", 
                                 "Le Morne"};
        
        for (int i = 0; i < cities.length; i++) {
            ArrayList<LinkedHashMap> list = (ArrayList) 
                feignClient.getDailyForecastAPI(token, cities[i]).get("data");
            for (int j = 0; j < list.size(); j++) {
                LinkedHashMap lmap = list.get(j);
                Double windSpeed = (Double) lmap.get("wind_spd");
                Double temp = Double.valueOf(lmap.get("temp").toString());
                if((5<temp) && (temp < 35) && (5<windSpeed) && (windSpeed < 18)){
                    if((windSpeed*3 + temp) > best){ 
                        forecast = new Forecast();
                        forecast.setDatetime(lmap.get("datetime"));
                        forecast.setCity(cities[i]);
                        forecast.setWindSpeed(lmap.get("wind_spd")+" m/s");
                        forecast.setTemp(lmap.get("temp")+ " Celcius");
                    }
                }
            }
        }
        return forecast;
    }
}

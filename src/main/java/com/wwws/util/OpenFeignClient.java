/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wwws.util;

import java.util.LinkedHashMap;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

//calling outer rest api
@FeignClient(name = "wwws-client", url = "${wwws.feign.clients.url}", fallback = HystrixClientFallback.class)
public interface OpenFeignClient {
    @GetMapping(value = "")
    public LinkedHashMap<String, Object> getDailyForecastAPI(
            @RequestParam(value = "key", required = true) String key,
            @RequestParam(value = "city", required = false) String city);
}

//response timeout and error handling
@Component
class HystrixClientFallback implements OpenFeignClient {
    @Override
    public LinkedHashMap<String, Object> getDailyForecastAPI(
            @RequestParam(value = "key", required = true) String key,
            @RequestParam(value = "city", required = false) String city){        
        return null;
    }
}
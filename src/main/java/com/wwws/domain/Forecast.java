/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wwws.domain;

import lombok.Data;

/**
 *
 * @author USER
 */
@Data
public class Forecast {
    
    private Object temp;
    private Object windSpeed ;
    private Object datetime;
    private String city;

}
